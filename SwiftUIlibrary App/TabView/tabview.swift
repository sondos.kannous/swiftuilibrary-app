//
//  tabview.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct tabView: View {
    @State private var selection: Tab = .tab1
    @State private var selection2: Tab1 = .tab3

       enum Tab {
           case tab1
           case tab2
       }
    enum Tab1 {
            case tab3
            case tab4
    }
    
    
    var body: some View {
        VStack{
            
        TabView(selection: $selection){
            
           Text("Tab1 .....").tag(Tab.tab1)
           Text("Tab2 .....").tag(Tab.tab2)
             
        }
        
        //this for can swipe between pages
        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
        
        TabView(selection: $selection2){
            
            Text("Tab3 .....").tabItem{ Label("Tab 3", systemImage: "star")}.tag(Tab1.tab3)
            Text("Tab4 .....").tabItem{ Label("Tab 4", systemImage: "list.star")}.tag(Tab1.tab4)
             
        }
        
            //we can use bottom bar with tabview by pass  $value
       
        
    
        }
}
    
}
