//
//  excelExportAndopen.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/3/23.
//

import SwiftUI
import UniformTypeIdentifiers
import GXQuickLookView

func getDataString() -> String{
    let heading = "  name  ,   email   ,   balance   \n"

    // For every element in recArray, extract the values associated with 'T' and 'F' as a comma-separated string.
    // Force-unwrap (the '!') to get a concrete value (or crash) rather than an optional
    let user1 =  User()
    user1.name = "sami"
    user1.balance = 300
    user1.email = "sami@user.com"
    let user2 =  User()
    user2.name = "hani"
    user2.balance = 400
    user2.email = "hani@user.com"
    let user3 =  User()
    user3.name = "rami"
    user3.balance = 250
    user3.email = "rami@user.com"
    let list :[User] = [user1, user2, user3]
    var rows = ""
    for i in list {
        rows +=  "\(i.name),\(i.email) ,\(i.balance) \n" }
        let csvString = heading + rows
        return csvString

}
class User {
    var userid:Int = 0
    var name:String = ""
    var email:String = ""
    var isValidUser:Bool = false
    var message:String = ""
    var balance:Double = 0.0
}

import SwiftUI
import UniformTypeIdentifiers
import GXQuickLookView

//view
struct ExportAndShowCSVFile: View {
    @State var isExporting: Bool = false
    @State private var showQuickLook: Bool = false
    @State private var previewURL: URL?

    var body: some View {
        VStack {
            Button("Show file in QuickLook") {
                showQuickLook.toggle()
            }
            .padding()
            .fullScreenCover(isPresented: $showQuickLook) {
                GXQuickLookView(url: $previewURL, isPresented: $showQuickLook)
            }

            Button("Export") {
                isExporting = true
            }
            .fileExporter(isPresented: $isExporting, document: CSVFile(initialText: getDataString()), contentType: UTType.commaSeparatedText, defaultFilename: "trasco report.csv") { result in
                switch result {
                case .success(let url):
                    previewURL = url
                    print("Saved to \(url)")
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}

struct CSVFile: FileDocument {
    // tell the system we support only plain text
    static var readableContentTypes = [UTType.commaSeparatedText]
    static var writableContentTypes = [UTType.commaSeparatedText]

    // by default our document is empty
    var text = ""

    // a simple initializer that creates new, empty documents
    init(initialText: String = "") {
        text = initialText
    }

    // this initializer loads data that has been saved previously
    init(configuration: ReadConfiguration) throws {
        if let data = configuration.file.regularFileContents {
            text = String(decoding: data, as: UTF8.self)
        }
    }

    // this will be called when the system wants to write our data to disk
    func fileWrapper(configuration: WriteConfiguration) throws -> FileWrapper {
        let data = Data(text.utf8)
        return FileWrapper(regularFileWithContents: data)
    }
}



// Modify the loadVideoFromDiskWith function to save the file in the temporary directory
func saveFileToTemporaryDirectory(data: Data, fileName: String) -> URL? {
    let temporaryDirectoryURL = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
    let fileURL = temporaryDirectoryURL.appendingPathComponent(fileName)
    
    do {
        try data.write(to: fileURL)
        return fileURL
    } catch {
        print("Error saving file to temporary directory: \(error)")
        return nil
    }
}
