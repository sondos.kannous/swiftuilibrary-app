//
//  localNotification.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/3/23.
//

import Foundation
import UserNotifications


func notificationRequestPermision() {
    
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
        if success {
            print("All simport UserNotificationset!")
        } else if let error = error {
            print(error.localizedDescription)
        }
    }
}



func sendNotificatonLocal(title : String ,subtitle: String ){

    let content = UNMutableNotificationContent()
    content.title = title
    content.subtitle = subtitle
    content.sound = UNNotificationSound.default

    // show this notification five seconds from now
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval:10, repeats: false)
    
    //for calander trigger //this send in first 9 hour or every if repeating
    var dateComponents = DateComponents()
    dateComponents.hour = 9
    _ = UNCalendarNotificationTrigger(dateMatching: dateComponents , repeats: true)

    
    
    
    // choose a random identifier
    let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)

    // add our notification request
   
    
    UNUserNotificationCenter.current().getNotificationSettings{ setting in
        if setting.authorizationStatus == .authorized {
            UNUserNotificationCenter.current().add(request)
        }
        
        else{
            
            
            notificationRequestPermision()
        }
        
    }

    print("sucess!!")
}
