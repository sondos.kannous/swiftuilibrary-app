//
//  localNotificationView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/3/23.
//

import SwiftUI

struct localNotificationView: View {
    var body: some View {
        VStack {
            Button("send local Notification") {
                sendNotificatonLocal(title: "my local notification......", subtitle: "yes its easy!!")
            }
        }
    }
}

struct localNotificationView_Previews: PreviewProvider {
    static var previews: some View {
        localNotificationView()
    }
}
