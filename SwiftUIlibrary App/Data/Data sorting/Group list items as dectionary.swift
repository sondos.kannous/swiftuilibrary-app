//
//  Group items from dectionary.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import Foundation
import SwiftUI





///for example I have list of products and I need group it by brand name
struct groupedView: View {
    
    var landmarks: [Landmark] = load("landmarkData.json")

    var categories: [String: [Landmark]] {
        Dictionary(
            grouping: landmarks,
            by: { $0.category.rawValue }
        )
    }
    
    var body: some View {
        NavigationView {
            ScrollView{
            VStack(alignment: .leading){
                ForEach(categories.keys.sorted(), id: \.self) { key in
                    Text(key)
                    ScrollView(.horizontal , showsIndicators: false){
                        
                    HStack{
                        
                    ForEach(categories[key]!){ value in
                    Text(value.name).padding().background(Rectangle().fill(.pink.opacity(0.4))).cornerRadius(8)
                            
                        }
                        
                    }
                    }
               
                 }
                Spacer()
            }.frame(
                width: 360,
                alignment: .leading).padding()
             .navigationTitle("Featured")
            }}
     }
}
