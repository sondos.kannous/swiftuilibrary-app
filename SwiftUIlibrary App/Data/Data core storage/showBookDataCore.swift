//
//  showBookDataCore.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI
import CoreData

struct showBookDataCore: View {
    
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(sortDescriptors: [
        
        //optional sorting
            SortDescriptor(\.rating,order: .reverse),
            SortDescriptor(\.name),
            
        ]
          //filter
          // ,predicate: NSPredicate(format: "rating == 3")
    )
    var books: FetchedResults<Book>
    
        @State var showAddAlert = false
     
        
        var body: some View {
            Spacer()
            
            Text(String(books.count))
            List{
                ForEach(books,id: \.self){ book in
                HStack{
                    Text(String (book.name ?? "not found")).bold().frame(width: 100)
                    Spacer()
                    RateBar(rate: Int(book.rating)).background(Rectangle().fill(.black).opacity(0.1).frame(width: 170, height: 30,      alignment: .center ).cornerRadius(10))
                    Spacer(minLength:5)
                }
                }
                .onDelete(perform: onTapDelete)}
            
            Button(action:{
                showAddAlert = true})
            {
            Image(systemName: "plus.square.on.square")
            }.padding(20).sheet(isPresented: $showAddAlert) {
                    AddBook()
                }
                
            EditButton() //need list 
            
        }
//
        func onTapDelete(at offsets: IndexSet){
            for offset in offsets{
            let book = books[offset]
                moc.delete(book)
               try? moc.save()

            }
        }
    }


//very important note : data core need real device this didnt work in simulator
