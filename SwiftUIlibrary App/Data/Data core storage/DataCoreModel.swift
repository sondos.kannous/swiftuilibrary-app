//
//  DataCoreModel.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import Foundation
import CoreData


class DataCoreController: ObservableObject{
    //THE NAME OF MY DATA MODEL
    let container = NSPersistentContainer(name: "book")
    init(){
        container.loadPersistentStores{
            desecription, error in
            if let error = error {
                print("core data failed to load: \(error.localizedDescription)")
            }
        }
    }
}



/*
 Steps to add local data base:
 1- file -> data model -> + Entity -> Add your attributes
 
 2- create ObservableObject for DataCoreController (link with last step file name) and init it
 
 3- create @stateObject in main and @environment in screen where I used it
 
 4- @fetchrequest for get and post
 
 */
