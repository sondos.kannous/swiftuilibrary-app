//
//  Addbook.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import Foundation
import SwiftUI

struct AddBook: View {
    //Add to date core
    @Environment(\.managedObjectContext) var moc
    @Environment(\.dismiss) var dissmis

    @State var name = ""
    @State var image = ""
    @State var rating = 5
    @State var description = ""

    var body: some View {

        VStack(spacing: 10){
            Group{
        TextField("NAME: ", text: $name)
        TextField("image: ", text: $image)
        TextField("description: ", text: $description)
            }.background(Rectangle().fill(.purple).opacity(0.05).frame(width: 500, height: 40, alignment: .center)).padding().cornerRadius(15)
            EditRateBar(rate: $rating).padding()
        Button(action:{
           let newbook = Book(context: moc)
            newbook.id = UUID()
            newbook.name = name
            newbook.image = image
            newbook.desription = description
            newbook.rating = Int16(rating)
            
            try? moc.save()
            dissmis()
        }){
            Text("Save")
        }
        }.padding()
        
        
    }
}

struct AddBook_Previews: PreviewProvider {
    static var previews: some View {
        AddBook()
    }
}
