//
//  fromJsonFileView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct fromJsonFileView: View {
    var landmarks: [Landmark] = load("landmarkData.json")
    
    var body: some View {
        VStack{
            
        Text("From Json File  .....")
            ScrollView{
        ForEach(landmarks, id:\.self){ landmarkitem in
            HStack{
          
            Text(landmarkitem.name)
        
            }
        }}
    }
}
}
struct fromJsonFileView_Previews: PreviewProvider {
    static var previews: some View {
        fromJsonFileView()
    }
}
