//
//  lettersAnimation.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct lettersAnimation: View {
    @State var offsetamount=CGSize.zero
    @State var enable=false
    let letters = Array("Coders District")
    
    
    
    var body: some View {
       
        HStack(spacing: 0){
            ForEach(0..<letters.count,id: \.self)
            {i in
                Text(String(letters[i])).padding(5).background(enable ? .pink : .purple).foregroundColor(.white).offset(offsetamount)
                    .animation(.default.delay(Double(i)/20), value:offsetamount )
            }.gesture(DragGesture()
                        .onChanged{
                offsetamount = $0.translation
            }
                        .onEnded{_ in
                offsetamount = .zero
                enable.toggle()
                
            }
            )
            
        }
    }
}

struct lettersAnimation_Previews: PreviewProvider {
    static var previews: some View {
        lettersAnimation()
    }
}
