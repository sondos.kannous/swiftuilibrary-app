//
//  HeroMatchesGeometryAnimation.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct HeroMatchesGeometryAnimation: View {
        
        var list = ["1","2","3"]
        @State var selectedItem : String = "1"
        @Namespace private var nameSpace
        var body: some View {
            
            HStack(spacing: 20){
                
            ForEach(list, id:\.self){item in
                ZStack{
                    if item == selectedItem {
                        Rectangle().fill( .pink.opacity(0.8)).frame(width:100)
                    
                     .cornerRadius(8)
                     .shadow(color: .white, radius: 2, x: 0, y: 0) .matchedGeometryEffect(id: "rec", in: nameSpace)
                      
                   
                    }
                    Text(item).padding().foregroundColor(.black).font(.largeTitle) .frame(width: 100, height: 60)
                    
                    
                }.frame( maxWidth:.infinity)
                .frame( height: 60).onTapGesture {
                    withAnimation(.spring()){
                        selectedItem =  item         }   }
            }
                
            }.padding()
        }
    }
