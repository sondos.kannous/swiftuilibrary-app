//
//  liskeVoiceRecorderScaleAnimation.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct likeVoiceRecorderScaleAnimation: View {
        @State var animationEffect:Double=1
        var body: some View {
            Circle().fill(.red).frame(width: 60, height: 60)
                .overlay(
                    
                    ZStack{
                        Image(systemName: "mic").frame(width: 30, height: 30).foregroundColor(.white)
                    Circle().stroke(.red).opacity(2-animationEffect).scaleEffect(animationEffect).animation(.easeOut(duration: 1).repeatForever(autoreverses: false), value: animationEffect)
                    }).onAppear(){
                    animationEffect = 2
                    }.onTapGesture{
                        print("clicked..............")
                    }
        }
    }
