//
//  GradiantrRadius.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import Foundation
import SwiftUI

struct RadialGradientView: View {
    @State var text:String=""
    var body: some View {
      
        
        VStack{
            RadialGradient(stops: [
                .init(color: .black, location: 0),
                .init(color: .accentColor, location: 0.4),
                    .init(color: .black, location: 0.7)
                
            ], center: .center, startRadius: 10, endRadius: 200)
            
        }
    }
}
