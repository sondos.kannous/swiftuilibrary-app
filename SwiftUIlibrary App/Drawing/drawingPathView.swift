//
//  drawingView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct drawingView: View {
        var body: some View {
            GeometryReader { geometry in
                     Path { path in
                         let width = min(geometry.size.width,geometry.size.height)
                         let middle = width * 0.5
                         path.addLines([

                            CGPoint(x:middle,y:middle),
                            CGPoint(x:middle+50,y:middle+100),
                            CGPoint(x:middle,y:middle+50),
                            CGPoint(x:middle-50,y:middle+100)
                        
                         
                         ]
                         )
                        // path.move(to: CGPoint(x:middle,y:middle+200))
            
                         path.addLines([
                            CGPoint(x:middle,y:middle+200),
                            CGPoint(x:middle+50,y:middle+300),
                            CGPoint(x:middle+130,y:middle+420),
                            CGPoint(x:middle-130,y:middle+420),
                            CGPoint(x:middle-50,y:middle+300),
                         ]
                         )
                         
                     }}}
    }
