//
//  deleteFromList.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct deleteFromList: View {
    
       @State var lines = [Int]()
       @State var counter:Int = 0
        
        var body: some View {
            VStack{
            List{
                ForEach(lines ,id: \.self
                ){
                    Text(String($0)).foregroundColor(.green)
                }.onDelete(perform: remove)
                
            }
                EditButton()
                
                Button("add row"){
                    counter = counter + 1
                    lines.append(counter)
                    
                }
                
            }
        }
        
        func remove(at offset: IndexSet){
            lines.remove(atOffsets: offset)

        }
    }



struct deleteFromList_Previews: PreviewProvider {
    static var previews: some View {
        deleteFromList()
    }
}
