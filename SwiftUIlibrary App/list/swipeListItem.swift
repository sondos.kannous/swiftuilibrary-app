//
//  swipeListItem.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/3/23.
//

import SwiftUI

struct swipeListItem: View {
    var body: some View {
        List{
            Text("swip me").swipeActions(){
                Button {
                    print("star")
                } label:{ Label("", systemImage: "star")}.tint(.yellow)
           
            }.swipeActions(edge: .leading){
                Button{
                    print("flower")
                } label: { Label("spring", systemImage: "fanblades")}.tint(.pink)
                
            }
            
            
            ForEach (1...10, id: \.self){ i in
                
                //this conditional should define here not in view code coz this heavy to swiftui view
                let imageName1 = i % 2 == 0 ? "moon.fill" : "bolt"
                let imageName2 = i % 2 == 0 ? "sparkles" : "ladybug"
                
                let tint1 = i % 2 == 0 ? Color.green : Color.purple
                let tint2 = i % 2 == 0 ? Color.red : Color.blue
                              
                
                Text(String(i)).swipeActions(){
                    
                    Button {
                        print("star")
                    } label:{ Label("", systemImage: imageName1)}.tint(tint1)
               
                }.swipeActions(edge: .leading){
                    Button{
                        print("flower")
                    } label: { Label("spring", systemImage: imageName2)}.tint(tint2)
                    
                }
            }
            
        }
    }
}

struct swipeListItem_Previews: PreviewProvider {
    static var previews: some View {
        swipeListItem()
    }
}
