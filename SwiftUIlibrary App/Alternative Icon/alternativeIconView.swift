//
//  alternativeIconView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/7/23.
//

import SwiftUI

struct alternativeIconView: View {

        var body: some View {
            VStack {

                            Button(action: {
                                setIcon(.firstIcon)
    
                            }) {
                                Text("Set 1 Icon")
                            }
    
                            Button(action: {
                                setIcon(.secondIcon)
                            }) {
                                Text("Set 2 Icon")
    
                        }
    
                Button(action: {
                
                    setIcon(.defaultIcon)
                }) {
                    Text("Set default Icon")
  
            }
        }
    }
}

struct alternativeIconView_Previews: PreviewProvider {
    static var previews: some View {
        alternativeIconView()
    }
}

//you can also change app icon from rempte config value

