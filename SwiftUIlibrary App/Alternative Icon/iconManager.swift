//
//  iconManager.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/7/23.
//

import Foundation
import UIKit


var currentAppIcon : NIAppIconType = .defaultIcon

enum NIAppIconType: CaseIterable {
    case defaultIcon,
         secondIcon,
         firstIcon
        
    
    init(name: String) { //Firebase icon name
        switch name {
        case "Test1":
            self = .firstIcon
        case "Test2":
            self = .secondIcon
       
        default:
            self = .defaultIcon
        }
    }
    
    var alternateIconName: String? { // given in info p-list
        switch self {
    
        case .firstIcon:
            return "Test1"
        case .secondIcon:
            return "Test2"
        
        case .defaultIcon:
            return nil
        }
    }
    
}


func setIcon(_ appIcon: NIAppIconType) {
        if UIApplication.shared.responds(to: #selector(getter: UIApplication.supportsAlternateIcons)) && UIApplication.shared.supportsAlternateIcons {
            
            typealias setAlternateIconName = @convention(c) (NSObject, Selector, NSString, @escaping (NSError) -> ()) -> ()
            
            let selectorString = "_setAlternateIconName:completionHandler:"
            
            let selector = NSSelectorFromString(selectorString)
            let imp = UIApplication.shared.method(for: selector)
            let method = unsafeBitCast(imp, to: setAlternateIconName.self)
            method(UIApplication.shared, selector, (appIcon.alternateIconName ?? "") as String as NSString, { _ in })
        }
    }



    // add CFBundleIcons then CFBundleAlternateIcons its so important in info plist: https://medium.com/naukri-engineering/how-to-achieve-dynamic-app-icon-in-ios-a2f13691af75
       // https://www.youtube.com/watch?v=coNGm5Km5k0


/*steps to add alternative icon:
 •    add images as file in project not in assets
 •    add images name in info plist
      - Icon files (iOS 5)  (Dictionary) ---> CFBundleAlternateIcons (Dictionary) ---> ImgName (Dictionary) ---> CFBundleIconFiles (Array)         ---> Item 0 ImgName
 •    iconManager had setIcon Function
 •    example view to show changes
 */

