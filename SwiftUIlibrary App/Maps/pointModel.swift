//
//  pointModel.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import Foundation
import CoreLocation

struct PointOfInterest: Identifiable {
  
    let id = UUID()
    let name: String
    let latitude: Double
    let longitude: Double
    let markerImageName: String
    

    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
