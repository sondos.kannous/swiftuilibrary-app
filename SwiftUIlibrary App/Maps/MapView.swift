//
//  MapView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import Foundation
import MapKit
import SwiftUI

struct MapView: View {
   var zoom = 0.07
    
    @State  private var region:MKCoordinateRegion
    init(){
        region = MKCoordinateRegion(
              center: CLLocationCoordinate2D(latitude: 33.5,
                                             longitude: 36.27),
              span: MKCoordinateSpan(latitudeDelta: zoom, longitudeDelta: zoom ))
        
    }
    
    private let places = [
        PointOfInterest(name: "location 1", latitude: 33.51, longitude:  36.27, markerImageName: "image1"),
            PointOfInterest(name: "location 2", latitude: 33.52, longitude: 36.28, markerImageName: "image2"),
            PointOfInterest(name: "location 3", latitude: 33.49, longitude: 36.26, markerImageName: "image3")
        ]
    var body: some View {
        ZStack{
            
        Map(coordinateRegion: $region , annotationItems: places){
            place in
            
            
            
            //for default amrker
           // MapMarker(coordinate: place.coordinate)
            
            //for image marker
            MapAnnotation(coordinate: place.coordinate) {
                               // Use a custom annotation view with a custom image
                                   Image(place.markerImageName) // Replace "custom_marker_image" with the name of your custom image
                                   .resizable()
                                   .frame(width: 30, height: 30)
                           }
        }
        .ignoresSafeArea(.all)
            VStack{
                
                Spacer()
                
                //Zoom in .... Zoom out
                HStack(spacing: 10){
                Circle().frame(width: 40, height: 40, alignment: .center).overlay(
                    ZStack{
                        Circle().fill(.pink)
                        Image(systemName: "minus").foregroundColor(.white)}
                ).onTapGesture {
                    region.span.latitudeDelta *= 1.5
                    region.span.longitudeDelta *= 1.5
                  
                }
                
                
                Circle().frame(width: 40, height: 40, alignment: .center).overlay(
                    ZStack{
                        Circle().fill(.pink)
                        Image(systemName: "plus").foregroundColor(.white)
                    }
                        
                ).onTapGesture {
                    region.span.latitudeDelta *= 1/1.5
                    region.span.longitudeDelta *= 1/1.5
                   
                }
            }
            }}
    
    }
    
}
