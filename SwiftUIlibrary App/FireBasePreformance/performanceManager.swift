//
//  performanceManager.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/13/23.
//

import Foundation
import SwiftUI
import FirebasePerformance


final class PerformanceManager{
    
 static let shared = PerformanceManager()
    private init (){}
    
    private var traces : [String : Trace] = [:]
    
    func startTrace(name: String){
        let trace = Performance.startTrace(name: name)
        traces[name] = trace
        
    }
    
    func setValue(name : String , value: String , forAttribute:String){
        guard let trace = traces[name] else { return}
        trace.setValue(value, forAttribute: forAttribute)
        traces.removeValue(forKey: name)
        
    }
    
    func stopTrace(name: String){
        guard let trace = traces[name] else { return}
        trace.stop()

        
    }
}
