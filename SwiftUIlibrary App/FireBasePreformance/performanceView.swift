//
//  performanceView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/13/23.
//

import SwiftUI

struct performanceView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/).onAppear{
            PerformanceManager.shared.startTrace(name: "Appearing")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                
                PerformanceManager.shared.setValue(name: "Appearing", value: "After2Minuts", forAttribute: "2Att")
                
            }
            
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                
                PerformanceManager.shared.stopTrace(name: "Appearing")
                
            }
        }
        .onDisappear{
           
            
        }
    }
}

struct performanceView_Previews: PreviewProvider {
    static var previews: some View {
        performanceView()
    }
}
