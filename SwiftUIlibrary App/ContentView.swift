//
//  ContentView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
     
        //--------------------------------------Design------------------------------------------------
        //tabView()
        
        // pickerView()
      
        //drawingView()   ///drawing with path
       
        // imageSliderView()
        
        //RadialGradientView()
        
        //stepperView()
    
        
        //---------------------------------------Data-----------------------------------------------------
      
        //groupedView()  ///for example I have list of products and I need group it by brand name
        
        //fromJsonFileView()    ///read data from json file
        
        //showBookDataCore() /// store local date base
        
        //ExportAndShowCSVFile()
       
        
        //---------------------------------------Animation-----------------------------------------------------
        //likeVoiceRecorderScaleAnimation()
        
        // lettersAnimation()
        
        //HeroMatchesGeometryAnimation()
        
        
        //---------------------------------------List-----------------------------------------------------
        // deleteFromList()
        
        //swipeListItem()
        
        
        
        //---------------------------------------Map-----------------------------------------------------
        //MapView()
        
       
        //---------------------------------------image picker-----------------------------------------------------
        //imagePickerView()
        
        
        //---------------------------------------local notification-----------------------------------------------------
        //localNotificationView()
        
        
        //---------------------------------------Rate-----------------------------------------------------
        // rateAppView()
        
        
       //---------------------------------------Performance----------------------------------------------
       // caheImagesView()
        
        
       //---------------------------------------Future----------------------------------------------
       //futureCombineView()
        
        
       //---------------------------------------Charts----------------------------------------------
       //barChartView()
        
        
     //---------------------------------------Alternative Icon App ----------------------------------------------
     // alternativeIconView()
        
    //--------------------------------------- FireBase ----------------------------------------------
//    remoteConfigView()
        performanceView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
