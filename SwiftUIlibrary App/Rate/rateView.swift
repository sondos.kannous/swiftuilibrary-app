//
//  rateView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import Foundation
import SwiftUI

struct RateBar: View {
   var rate: Int
    var body: some View {
        HStack{
            ForEach(1...5,id: \.self){
                num in
                Image(systemName: rate >= num ? "star.fill" : "star").foregroundColor(.yellow)
            }
        }
    }
}

struct EditRateBar: View {
   @Binding var rate: Int
    var body: some View {
        HStack{
            ForEach(1...5,id: \.self){
                num in
                Image(systemName: rate >= num ? "star.fill" : "star").foregroundColor(.yellow).onTapGesture {
                    rate = num
                }
            }
        }
    }
}
