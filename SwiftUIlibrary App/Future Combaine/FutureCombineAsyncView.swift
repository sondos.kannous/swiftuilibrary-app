//
//  FutureCombineAsyncView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/6/23.
//

import Foundation
import SwiftUI
import Combine

struct futureCombineView: View {
   @State  var list : [String] = []
   @State var cancelable : [AnyCancellable] = []
    var action = PassthroughSubject<String,Never>()
    
    var body: some View {
        
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/).onAppear{
            futureCallApi.shared.fetchdata().sink(
                receiveCompletion: {completion in
                switch completion {
                case .finished: print("finished")
                case .failure(let error) : print("\(error)")
                    }
                    
                },
                receiveValue: {
                 value in
                    self.list = value
                    
                }).store(in: &cancelable)
            //listen to action
            action.sink{
                string in
                print(string)
            }.store(in: &cancelable)
          
         //when do action
        }.onTapGesture {
            action.send("7")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                action.send("8")}
            DispatchQueue.main.asyncAfter(deadline: .now() + 4){
                action.send("9")}
           
        }.onReceive(action) { item in
            list.append(item)
        }
        
        if(list.isEmpty){
         ProgressView()
        }
        else{
        VStack{
            ForEach( list ,id: \.self){item in
                
                Text(item)
            }
        }
            
        }
    }
}

class futureCallApi{
    
 static var shared = futureCallApi()
    
    func fetchdata () -> Future<[String],Error> {
        return Future { promise in
            
         DispatchQueue.main.asyncAfter(deadline: .now() + 3){
         promise(.success(["1","2","3","4","5","6"]))
        }
        }
    }
    
}
