//
//  imageSliderView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct imageSliderView: View {
    let images = ["image1", "image2", "image3", "image4"] // Replace with your image data
   
       var body: some View {
           VStack {
               ImageSliderView(images: images)
                   .frame(height: 500)
               
               // Add other content below the image slider
           }
       }
}

struct imageSliderView_Previews: PreviewProvider {
    static var previews: some View {
        imageSliderView()
    }
}

struct ImageSliderView: View {
    let images: [String] // Replace String with your image data type
    @State private var selectedTab = 0
    let timer = Timer.publish(every: 5, on: .main, in: .common).autoconnect()
    var body: some View {
        VStack{
        TabView(selection: $selectedTab) {
            ForEach(images.indices, id: \.self) { index in
                Image(images[index])
                    .resizable()
                    .scaledToFit()
                    .tag(index)
            }
        }
        .tabViewStyle(PageTabViewStyle()) // Use PageTabViewStyle for an image slider effect
        .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always )) // Show the page indicators
        
        
        //for more control for circles index can make backgroundDisplayMode = .never and add this Circles
        Circles(index: $selectedTab, count: images.count, color: .purple)
        }.onReceive(timer) { _ in
            // Move to the next image when the timer triggers
            withAnimation{
                selectedTab = (selectedTab + 1) % images.count}
        }
    }
    
}


struct Circles: View {
    @Binding var index : Int
    var count :Int
    var color: Color = Color.pink
    var body: some View {
        HStack{
            ForEach(0...count-1 , id: \.self){
                i in
                Circle().fill( index == i ? color : Color.gray ).frame(width: 8, height: 8)
                    .onTapGesture{
                        withAnimation{
                            index = i}
                    }
            }
        }
    }
}
