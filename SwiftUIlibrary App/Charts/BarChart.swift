//
//  BarChart.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/6/23.
//

import Foundation
import SwiftUI

struct barChartView: View {
  
    var list : [Double] = [80,400 , -100 , 0 , 1000 , -120 , 500, -500]
    var max : Double = 0
    var min : Double = 0
    var all : Double = 0
    init(){
        max = list.max() ?? 0
        min = list.min() ?? 0
        all = max + abs(min)
        
    }
    
    var body: some View {
        VStack{
         HStack(alignment: .bottom){
         ForEach(list , id:\.self ){ i in
         Rectangle().fill(i > 0 ? .blue : .red ).frame(width: 20, height: i == 0 ? 1 : abs(CGFloat(i) * (200 / all) ))
         .offset(y: i > 0 ? 0 :  CGFloat(i) * -(200 / all) )
            }
        }
        Spacer()
        }.frame( height: 200)
            .onAppear{
                let max = list.max()
                print(max ?? 0)
                let min = list.min()
                print(min ?? 0)
                
            }
       
         //   .rotation3DEffect(.degrees(180), axis: (x: 0, y: 1, z: 0))
//
////
        }

    
    }
