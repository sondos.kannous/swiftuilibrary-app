//
//  SwiftUIView.swift
//  SwiftUIlibrary App

//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct pickerView: View {
    
    @State var seasion:Seasion = .winter
    var body: some View {
        VStack{
        Picker("Seasonal Photo", selection: $seasion) {
            ForEach(Seasion.allCases,id:\.self) { season in
                               Text(season.rawValue)
                           }
                       }
        .pickerStyle(.wheel)
        
        
        Picker("Seasonal Photo", selection: $seasion) {
            ForEach(Seasion.allCases,id:\.self) { season in
                               Text(season.rawValue)
                           }
                       }
        .pickerStyle(.inline)
     
        
        Picker("Seasonal Photo", selection: $seasion) {
            ForEach(Seasion.allCases,id:\.self) { season in
                               Text(season.rawValue)
                           }
                       }
        .pickerStyle(.menu)
        
    
    Picker("Seasonal Photo", selection: $seasion) {
        ForEach(Seasion.allCases,id:\.self) { season in
                           Text(season.rawValue)
                       }
                   }
    .pickerStyle(.segmented)
    }
    }
}


//CaseIterable for can use .allCases
enum Seasion:String,CaseIterable{
    case spring="🌸"
    case winter="❄️"
    case summer="🔆"
    case fall="🍂"
}
