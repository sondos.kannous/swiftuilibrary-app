//
//  imagePicker.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

struct imagePickerView: View {
    @State var showImagePicker: Bool = false
    @State var showselectPicker: Bool = false
    @State var image: Image? = nil
    @State var isCamera = false

        var body: some View {
            ZStack {
                VStack {
                    Button(action: {
                        self.showselectPicker.toggle()
                    }) {
                        Text("Show image picker")
                    }
                    image?.resizable().frame(width: 200, height: 200)
                }
                .sheet(isPresented: $showselectPicker  ){
                    ZStack{
                      Color.gray.edgesIgnoringSafeArea(.all)
                        
                    VStack{
                    Spacer()
                    if(showImagePicker){
                        FromGallery(image: $image,showImagePicker: $showImagePicker, fromCmera: $isCamera)
                    }
                    else {
                  
                        VStack{
                            Text("Image Picker").onTapGesture{
                                showImagePicker = true
                                isCamera = false
                            }
                        Divider()
                        
                            Text("camera").onTapGesture{
                                showImagePicker = true
                                isCamera = true
                            }
                            
                        
                            
                        }.frame(maxWidth:.infinity, maxHeight: 200).background(Rectangle().fill(.white).cornerRadius(40, corners: [.topLeft,.topRight]))}
                        
                    }
                   
                
                    }.edgesIgnoringSafeArea(.all)
                }
                        
                    //to prefix sheet height and width use .presentationDetents([.height(200)]) in content of sheet

                }
            }
        }

    
    struct FromGallery: View {
        @Binding var image : Image?
        @Binding var showImagePicker :Bool
        @Binding var fromCmera : Bool
        var body: some View {
            ImagePicker(sourceType: fromCmera ? .camera : .photoLibrary) { image in
            self.image = Image(uiImage: image)
                showImagePicker = false
            
            }
        }
    }


