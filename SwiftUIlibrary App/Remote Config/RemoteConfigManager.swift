//
//  RemoteConfigManager.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/7/23.
//

import Foundation
import Firebase

struct RemoteConfigManager{
    
    private static var remoteConfig : RemoteConfig = {
        var remoteConfig =  RemoteConfig.remoteConfig()
        //Keep in mind that this setting should be used for development only, not for an app running in production. If you're just testing your app with a small 10-person development team, you are unlikely to hit the hourly service-side quota limits. But if you pushed your app out to thousands of test users with a very low minimum fetch interval, your app would probably hit this quota.
        
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0 // you can use 0 second only for develope reason not for app production the default value is 12 hour
        remoteConfig.configSettings = settings
        remoteConfig.setDefaults(fromPlist: "properityList")
        return remoteConfig
    }()
    
    static func configure(exprationDuration : TimeInterval =  0.0){
        
        remoteConfig.fetch(withExpirationDuration: exprationDuration){
            (status , err) in
            if let err = err {
                print("1")
                print(err.localizedDescription)
                return
            }
            
            print("recived value from remote config......")
            RemoteConfig.remoteConfig().activate(completion: nil)
            
            
            //to change app icon with remote config
            if( RemoteConfigManager.value(forKey: RCKey.iconNumber) == 1){
                setIcon(.firstIcon)
                print("1")
                
            }
            else if (RemoteConfigManager.value(forKey: RCKey.iconNumber) == 2){
                setIcon(.secondIcon)
                print("2")
            }
            else {
                setIcon(.defaultIcon)
                print("3")
            }
            
            
        }
        
    }
    
    static func value(forKey key : String) -> String{
        
        return remoteConfig.configValue(forKey: key).stringValue!
    }
    static func value(forKey key : String) -> Int{
        
        return Int(truncating: remoteConfig.configValue(forKey: key).numberValue)
    }
}


/*
    •    import firebase including remote config
    •    add app delegate file to app and configure firebase and remoteConfigManager in it
    •    link with app delegate in main
    •    remote config manager (include change image from remote config number value)
    •    RCKey with propertyList for default value
    •    example view to show remote config result
 */
