//
//  AppDelegate.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/7/23.
//

import Foundation
import UIKit
import Firebase



class AppDelegate: NSObject, UIApplicationDelegate {
func application(_ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    FirebaseApp.configure()
    RemoteConfigManager.configure(exprationDuration: 0)

    return true
}
}
