//
//  SwiftUIlibrary_AppApp.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI

@main
struct SwiftUIlibrary_AppApp: App {
    
    
    //for data core local storage model
    //Note: dont forget .environment(\.managedObjectContext,dataCoreController.container.viewContext)
    @StateObject private var dataCoreController = DataCoreController()

    
    // register app delegate for Firebase setup (for remote config)
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    var body: some Scene {
        WindowGroup {
            ContentView().environment(\.managedObjectContext,dataCoreController.container.viewContext)
        }
    }
}
