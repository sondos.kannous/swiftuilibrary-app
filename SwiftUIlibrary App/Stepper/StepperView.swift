//
//  StepperView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/6/23.
//

import SwiftUI

struct stepperView: View {
    @State  var value: Int = 0

    var body: some View {
        VStack (alignment: .leading) {
            Text("\(value)").accessibilityLabel("countLabel")
            Stepper("", value: $value, in: 0...7)
        }.frame(width: 300)
    }
    func increment() {
        self.value += 1
    }
}

