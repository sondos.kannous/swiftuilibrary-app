//
//  cacheImageWebViewModel.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/6/23.
//

import Foundation
import SwiftUI
import SDWebImageSwiftUI

class CachImageWebViewModel: ObservableObject {
    var url = "https://th.bing.com/th/id/R.4ca1feb133983eb834d74c60f843562d?rik=TqwxvqGWKxcwkA&pid=ImgRaw&r=0"
    @Published var startingImageURL: URL? = nil
    @Published var cachedImage: UIImage? = nil
    let imageName: String = "charleyrivers"
    let manager = CachManager.instance

    init() {
        getImageFromWeb()
    }

    func getImageFromWeb() {
        guard let imageURL = URL(string: url) else { return }
        startingImageURL = imageURL
    }

    func saveToCache() {
        guard let startingImageURL = startingImageURL else { return }

        // Use SDWebImage to download the image from the web URL
        SDWebImageDownloader.shared.downloadImage(with: startingImageURL) { [weak self] (image, _, _, _) in
            guard let self = self, let image = image else { return }

            // Convert the downloaded image to UIImage and save it to the cache
            self.manager.add(image: image, name: self.imageName)
        }
    }

    func removeFromCache() {
        manager.remove(name: imageName)
    }

    func getFromCache() {
        cachedImage = manager.getCache(name: imageName)
    }
}
