//
//  cacheImageLocalViewModel.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/6/23.
//

import Foundation
import SwiftUI



class CacheImageLocalViewModel : ObservableObject{

    @Published var startingImage : UIImage? = nil
    @Published var cachedImage : UIImage? = nil
    let ImageName : String = "image1"
    let manager = CachManager.instance

    init(){
        getImageFromAssetsFolder()
    }

    func getImageFromAssetsFolder(){
    startingImage = UIImage(named: ImageName)
    }

    func saveToCache(){
        guard let image = startingImage else{ return}
        manager.add(image: image, name: ImageName)

    }
    func removeFromCache(){
        manager.remove(name: ImageName)

    }
    func getFromCache(){

        cachedImage = manager.getCache(name:ImageName)

    }

}
