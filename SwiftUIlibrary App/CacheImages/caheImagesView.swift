//
//  caheImagesView.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/6/23.
//

import SwiftUI
import SDWebImageSwiftUI

struct caheImagesView: View {
    @StateObject var vmWeb = CachImageWebViewModel()
    @StateObject var vmLocal = CacheImageLocalViewModel()
    var body: some View {
        HStack{
            VStack{
            Text("web")
           if let startingImageURL = vmWeb.startingImageURL {
                        // Load and display the web image using WebImage view
                        WebImage(url: startingImageURL)
                            .resizable()
                            .frame(width: 100, height: 150)
                    }

                    Text("Save on Cache")
                        .padding()
                        .frame(height: 30)
                        .background(Color.green.opacity(0.8))
                        .cornerRadius(8)
                        .onTapGesture {
                            vmWeb.saveToCache()
                        }

                    Text("Remove from Cache")
                        .padding()
                        .frame(height: 30)
                        .background(Color.red.opacity(0.8))
                        .cornerRadius(8)
                        .onTapGesture {
                            vmWeb.removeFromCache()
                        }

                    Text("Get from Cache")
                        .padding()
                        .frame(height: 30)
                        .background(Color.blue.opacity(0.8))
                        .cornerRadius(8)
                        .onTapGesture {
                            vmWeb.getFromCache()
                        }

                    if let image = vmWeb.cachedImage {
                        // Display the cached image using the UIImage received from the cache
                        Image(uiImage: image)
                            .resizable()
                            .frame(width: 100, height: 150)
                    }
                
                
                
            }
            
            VStack{
                Text("local")
                        if let image = vmLocal.startingImage{
                            Image(uiImage: image).resizable().frame(width: 100, height: 150)
                        Text("Save on Cache").padding().frame(height: 30).background(.green.opacity(0.8))
                                .cornerRadius(8).onTapGesture {
                                    vmLocal.saveToCache()
                                }
                        Text("Remove from Cache").padding().frame( height: 30).background(.red.opacity(0.8))
                                .cornerRadius(8).onTapGesture {
                                    vmLocal.removeFromCache()
                                }
                        Text("get from Cache").padding().frame( height: 30).background(.blue.opacity(0.8))
                                    .cornerRadius(8).onTapGesture {
                                        vmLocal.getFromCache()
                                    }
                            if let image = vmLocal.cachedImage{
                                Image(uiImage: image).resizable().frame(width: 100, height: 150)}
                    }
            }
            
            
            
        }
    }
}

struct caheImagesView_Previews: PreviewProvider {
    static var previews: some View {
        caheImagesView()
    }
}
