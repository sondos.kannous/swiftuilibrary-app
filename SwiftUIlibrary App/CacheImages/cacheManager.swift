//
//  cacheManager.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/6/23.
//

import Foundation

import SwiftUI



class CachManager{
    static let instance = CachManager()
    private init(){
        
    }
    var imageCache: NSCache<NSString , UIImage> = {
        let cache = NSCache<NSString , UIImage> ()
        cache.countLimit = 10
        cache.totalCostLimit = 1024 * 1024 * 100 //100 mb
        return cache
    }()
    
    func add (image: UIImage , name: String){
        imageCache.setObject(image, forKey: name as NSString)
        print("added to cache!")
    }
    func remove( name: String){
        imageCache.removeObject(forKey: name as NSString)
        print("remove cache!")
    }
    
    func getCache( name: String)-> UIImage?{
       
        return imageCache.object(forKey: name as NSString)
   
    }
}
